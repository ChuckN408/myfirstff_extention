const ZOOM_INCREMENT = 0.2;
const MAX_ZOOM = 3;
const MIN_ZOOM = 0.3;
const DEFAULT_ZOOM = 1;

else if (e.target.id === "tabs-add-zoom") {
	callOnActiveTab((tab) => {
		var gettingZoom.then((zoomFactor) => {
			//max zoom is 3, cant go higher
			if (zoomFactor >= MAX_ZOOM) {
				alert("Tab zoom is at max!");
			} else {
				var newZoomFactor = zoomFactor + ZOOM_INCREMENT;
				//if the newzoom is set to higher, then max is accepted
				newZoomFactor = newZoomFactor > MAX_ZOOM ? MAX_ZOOM : newZoomFactor;
				browser.tabs.setZoom(tab.id, newZoomFactor);
			}
		});
	});
}
