var bgBtns = document.querySelectorAll('.bg-container button');

for(var i = 0; i < bgBtns.length; i++) {
	var imgName = bgBtns[i].getAttribute('class');
	var bgImg = 'url(\'images/' + imgName + '.png\')';
	bgBtns[i].style.backgroundImage = bgImg;
	bgBtns[i].onclick = function(e) {
		cookieVal.image = fullURL;
		browser.cookies.set({
			url: tabs[0].url,
			name: "bgpicker",
			value: JSON.stringify(cookieVal)
		})

		var reset = document.querySelector('.color-reset button');

		browser.cookies.onChanged.addListener((changeInfo) => {
			console.log(`Cookie changed:\n
				* Cookie: ${JSON.stringify(changeInfo.cookie)}\n
				* Cause: ${changeInfo.cause}\n
				* Removed: ${changeInfo.removed}`);
			});
