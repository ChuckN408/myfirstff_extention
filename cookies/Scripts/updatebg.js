gettingCookies.then((cookie) => {
	if (cookie) {
		var cookieVal = JSON.parse(cookie.value);
		browser.tabs.sendMessage(tabs[0].id, {image: cookieVal.image});
		browser.tabs.sendMessage(tabs[0].id, {color: cookieVal.color});
	}
});
