var gettingActiveTab = browser.tabs.query({active: true, currentWindow: true});
gettingActiveTab.then(updateTab);

function updateTab(tabs) {
	if (tabs[0]) {
		currentTab = tabs[0];
		if (isSupportedProtocol(currentTab.url)) {
			function isSupportedProtocol(urlString){
				var supportedProtocols = ["https:", "http:", "ftp:", "file:"];
				var url = document.createElement('a');
				url.href = urlString;
				return supportedProtocols.indexOf(url.protocol) != -1;
			}

			var searching = browser.bookmarks.search({url: currentTab.url});
			searching.then((bookmarks) => {
				currentBookmark = bookmarks[0];
				updateIcon();

				function updateIcon() {
					browser.browserAction.setIcon({
						path: currentBookmark ? {
							19: "icons/star-filled-19.png",
							38: "icons/star-filled-38.png"
						} : {
							19: "icons/star-empty-19.png",
							38: "icons/star-empty-38.png"
						}
						tabId: currentTab.id
					});
					browser.browserAction.setTitle({
						// Screen readers can see the title
						title: currentBookmark ? 'Unbookmarking it...' : 'Go mark it',
						tabId: currentTab.id
					});
				}

				browser.browserAction.onClicked.addListener(toggleBookmark);

				function toggleBookmark() {
					if (currentBookmark) {
						browser.bookmarks.remove(currentBookmark.id);
					} else {
						browser.bookmarks.create({title: currentTab.title, url: currentTab.url});
					}
				}

				//listen for booksmarks being created
				browser.bookmarks.onCreated.addListener(updateActiveTab);
				//'			' removed
				browser.bookmarks.onRemoved.addListener(updateActiveTab);
				//listen tab url changes
				browser.tabs.onUpdated.addListener(updateActiveTab);
				//listen tab switching
				browser.tabs.onActivated.addListener(updateActiveTab);
				//listen window switch
				browser.windows.onFocusChanged.addListener(updateActiveTab);

			}
		}
	}
}

