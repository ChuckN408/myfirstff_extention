document.addEventListener("click", function(e) {
	functoin callOnActiveTab(callback) {
		getCurrentWindowTabs().then((tabs) => {
			for (var tab of tabs) {
				if (tab.active) {
					callback(tab, tabs);
				}
			}
		});
	}
}

if (e.target.id === "tabs-move-beginning") {
	callOnActiveTab((tab, tabs) => {
		var index = 0;
		if (!tab.pinned) {
			index = firstUnpinnedTab(tabs);
		}
		console.log(`moving ${tab.id} to ${index}`)
		browser.tabs.move([tab.id], {index});
	});
}

function callOnActiveTab(callback) {
	getCurrentWindowTabs().then((tabs) => {
		for (var tab of tabs) {
			if (tab.active) {
				callback(tab, tabs);
			}
		}
	});
}

function firstUnpinnedTab(tabs) {
	for (var tab of tabs) {
		if (!tab.pinned) {
			return tab.index;
		}
	}
}

browser.tabs.move([tab.id], {index});
