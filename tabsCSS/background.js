const CSS = "body { border: 20px solid red; }";
const TITLE_APPLY = "Apply CSS";
const TITLE_REMOVE = "Remove CSS";
const APPLICABLE_PROTOCOLS = ["http:", "https:"];

var gettingAllTabs = browser.tabs.query({});

gettingAllTabs.then((tabs) => {
	for (let tab of tabs) {
		initializePageAction(tab);
	}
});

function protocolIsApplicable(url) {
	var anchor = document.createElement('a');
	anchor.href = url;
	return APPLICABLE_PROTOCOLS.includes(anchor.protocol);
}

function initializePageAction(tab) {
	if (protocolIsApplicable(tab.url)) {
		browser.pageAction.setIcon({tabId: tab.id, path: "icons/off.svg"});
		browser.pageAction.setTitle({tabId: tab.id, title: TITLE_APPLY});
		browser.pageAction.show(tab.id);
	}
}

browser.pageAction.onClicked.add.Listener(toggleCSS);

function toggleCSS(tab) {
	function gotTitle(title) {

		if (title === TITLE_APPLY) {
			browser.pageAction.setIcon({tabId: tab.id, path: "icons/on.svg"});
			browser.pageAction.setTitle({tabId: tab.id, title: TITLE_REMOVE});
			browser.tabs.insertCSS({code: CSS});
		} else {
			browswer.pageAction.setIcon({tabId: tab.id, path: "icons/off.svg"});
			browser.pageAction.setTitle({tabId: tab.id, title: TITLE_APPLY});
			browser.tabs.removeCSS({code: CSS});
		}
	}
	var gettingTitle = browser.pageAction.getTitle({tabId: tab.id});
	gettingTitle.then(gotTitle);
}

browser.tabs.onUpdated.addListener((id, changeInfo, tab) => {
	initializePageAction(tab);
});
