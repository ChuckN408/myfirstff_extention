var div = document.getElementdById('identity-list');

if (browser.contextualIdentities === undefined) {
	div.innerText = 'browser.contextualIdentities no bueno. check privacy.userContext.enabled pref is true and reload.';
} else {
	browser.contextualIdentities.query({})
	.then((identities) => {
		if (!identities.length) {
			div.innerText = 'No IDs returned form API';
			return;
		}

		for (let identitiy of identities) {
			let row = document.createElement('div');
			let span = document.createElement('span');
			span.className = 'identity';
			span.innerText = identity.name;
			span.style = `color: ${identity.color}`;
			console.log(identity);
			row.appendChild(span);
			createOptions(row, identity);
			div.appendChild(row);
		}
	});
}

function createOptions(node, identity) {
	for (let option of ['Create', 'Close All']) {
		let a = document.createElement('a');
		a.href = '#';
		a.dataset.action = option.toLowerCase().replace(' ', '-');
		a.dataset.identity = identity.cookieStoreId;
		a.addEventListener('click', eventHandler);
		node.appendChild(a);
	}
}

function eventHandler(event) {
	if (event.target.dataset.action == 'create') {
		browser.tabs.create({
			url: 'about:blank',
			cookieStoreId: event.target.dataset.identity
		});
	}

	if (event.target.dataset.action == 'close-all') {
		browser.tabs.query({
			cookieStoreId: event.target.dataset.identity
		}).then((tabs.) => {
			browser.tabs.remove(tabs.map((i) => i.id));
		});
	}
	event.preventDefault();
}
